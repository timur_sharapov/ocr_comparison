import os

vl1 = ["26.09.2017","35841","8481.9090","432636140627435"]
vl2 = ['104511','68305','DDP','3204.1200','CHE-107.953.261','UBSWCHZH80A','Suisse']
vl3 = ['68305','206232','France','DDP','FR94485196323','3204.1600','Suisse','Inde']
vl4 = ['4525980','05.09.2017','CHE101174465','17049071','Germany']
vl5 = ['316233165','DE113055856','CHE-106.032.803','4508271918','44152000','30049000','CIP']
vl6 = ['992010721','28.09.2017','30021200','USA','1563146']
vl7 = ['90253095','07.02.2018','1131476','201794','22011019','22011011']
vl8 = ['DE172338469','CHE-105.835.768','233034','538123','EXW','9025.8000','DE4669495']
vl9 = ['4060828074','ICC0117','76042100','European Union','CPT','06.02.2018']
vl10 = ['90000239','07.02.2018','DE85','DAP','30021200','4000000786']

answers = [vl1,vl2,vl3,vl4,vl5,vl6,vl7,vl8,vl9,vl10]

def check_score():
    txt_file = open("./Logs/output_dpi.txt","w+")
    variable_list = dict(zip([str(item) for item in range(1, 11)], answers))
    base_directory = "./"
    must_see = ["Azure_Results_DPI", "Tesseract_Results_DPI", "IronOCR_Results_DPI","Vision_Results_DPI"]

    for item in must_see:
        engine = item.split('_')[0]
        total_score = 0
        source_dir = os.path.join(base_directory, item)
        list_of_files = os.listdir(source_dir)
        for folder in list_of_files:
            files_with_texts = os.listdir(os.path.join(source_dir, folder))
            for f in files_with_texts:
                dpi_score = 0
                if f.endswith('.txt'):
                    with open(os.path.join(source_dir, folder, f), encoding='utf-8') as file:
                        data = file.read().replace('\n', '')
                        for answer in variable_list[folder]:
                            if (answer in data):
                                total_score += 1
                                dpi_score += 1
                txt_file.write("ENGINE :" + engine + " FILE :" + folder + " DPI: " + f + 
                      " SCORE: " +  str(dpi_score) + " OF " + str(len(variable_list[folder])) + "\n")
            txt_file.write("\n")
        txt_file.write("____________________________________________________\n")
        txt_file.write("ENGINE: " + engine + " TOTAL SCORE: " + str(total_score) + "\n")
        txt_file.write("____________________________________________________\n\n")
    txt_file.close()

if __name__ == "__main__":
    check_score()