giifits

Die ganze Welt der Werbeartikel

iffits G Be 122 b. 22083

B1 Swiss Gastro GmbH
Zentrallager
Althardstrasse 10

8105 Regensdorf
Switzerland

    

Bonbons im Werbewickel
Artikelnr.: 14079

 

Lieferdatum: 22.09.2017

Zolltarifnummer: 17049071

Bonbon im Werbewickel aus glasklarem alternativ weiBem Zellglas einzeln
gewickelt. Format: offen ca. 80 x 60 mm Geschmack: ab 25 kg Fruchtmischung
aus Zitrone, Orange, Apfel, Kirsche und Cassis. Haltbarkeit: ca. 18 Monate bei
sachgerechter Lagerung Verpackung: ca. 190 StUck / kg, 1 kg / Beutel, 5 kg / Kar
ton Lieferzeit: ca. 15 Arbeitstage nach Druckfreigabe. Express-Lieferung auf

Anfrage! Preise inkl. 3-Farbdruck, zzgl. Nebenkosten.

Geschmack: Fruchtmischung
Veredlung / ohne V.: 3-farbig

Ruestkosten
Artikelnr.: V3550548

Logo Deperado

Lieferdatum: 22.09.2017

Druckfarben 3405C grin, magenta, schwarz

Unverdanderter KlischeeNr, 61722

 

Frachtkosten

Artikelnr.: TO Lieferdatum: 22.09.2017
Firmenname: Giffits GmbH Tel.:
Geschaftsfthrer: Thorsten Schmidt, Marcus Schulz Fax.:
Handeisregister: HRB 106954, Hamburg E-Mail:
Gerichtstand, Erfililungsort: Hamburg-Barmbek, Germany Steuer-Nr.:
WEEE-Reg.Nr.: DE68758991 Ust-id-Nr.:

_ Proforma-Rechnung
Belegnummer Datum Seite .
4525980 05.09.2017 1/2
Kundennummer UID-Geschaftspartner
B156532 CHE101174465

Ihr Zeichen

 

Referenz

ihr Ansprechpartner

Marcus Kelkel (marcus.kelkel@giffits.de)
Tel.: +49-40-2788201-28 Fax: 043 - 508 01 36

Rechnungsempfanger
B1 Swiss Gastro GmbH

Mirja Classen
Limmattalstrasse 215

8049 Zurich
Switzerland

aprels, se Total EUR.

2.000 4,1200 8.240,00

Ursprungsland: Germany (Baden-Wurttemberg)

1 78,0000 78,00

1 380,0000

043 - 550 15 30
043 - 508 01 36
service@giffits.ch
43/724/00701
DE262054425

Bankverbindung:
Swift/BIC-Code:
iBAN-Nummer:

Commerzbank AG Zurich
COBACHZHXXX
CH51 0883 6120 6077 0000 6
