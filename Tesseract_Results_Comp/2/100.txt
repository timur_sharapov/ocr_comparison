feo OF 4 OCR

Metzger+ Richner Transport

AAV

Représentant Fiscal de

SYNCO CHEMIE

AG

Rue des Transitaires
F-68305 Saint-Louis

25/09/2017

LEATHER

ELLISYNCO

AVELLIS SYNCO LEATHER (SWISS) AG ° Langmattweg 3 ° CH-4466 Ormalingen ° Switzerland

98

ps 1

Anne-Catherine Rosenbaum

France

Order No. 104511

FACTURE No. 18515

Votre commande:15/09/2017 N. Réf:

Votre Réf: ACR

Conditions de livraison: DDP WERLE a THANN, par camion.

Conditions du paiement: 30 jours net

No.Art. Description Colis

Lot Tarif douanier Quantité

8785-86 Resacor Bleu FR 2.000

*

116091 ) AIK AVS 3204.1200 50.000
Origine : Italie

9365-86 Resacor Bleu Marine T 6.000

117075 3204.1200 150.000
Origine : Suisse, non préf.

8635-86 Resacor Ecarlate B 4.000

117041 3204.1200 100.000
Origine : Suisse, non préf.

8745-86 Resacor Violet 2R 2.000

117039 3204.1200 50.000
Origine : Suisse, non préf.

9386-249 Resacor Noir lumiére PL liquide 24.000

636 3204.1200 720.000

Tel +41 61 985 10 45 Bank: UBS AG, CH-4410 Liestal, Switzerland

Fax +41 61 981 16 30 CHF: 233-26262022.0 (IBAN CH86 0023 3233 2626 2022 0)

EUR: 233-26262022.8 (IBAN CH64 0023 3233 2626 2022 8)
USD: 233-26262022.4 CIBAN CH75 0023 3233 2626 2022 4)

info@avellis-synco.ch BIC/SWIFT: UBSWCHZH80A

Carton
kg

Carton
kg

carton
kg

Carton
kg

Kanister
kg

Prix / kg
Montant EUR

EUR 15.50

EUR 775.00

EUR 12.50

EUR 1875.00

EUR 8.50
EUR 850.00

EUR 10.35
EUR 517.50

EUR 3.90
EUR 2808.00

CHE-107.953.261 MwST

Europe Tax ID: DE 811 456 182

Iso 9001:2015 certified
