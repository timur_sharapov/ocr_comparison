ro)

A.

HANSA'HEEMANN

HANSA-HEEMANN AG . Postfach 1353 . 25454 Rellingen

ALDI SUISSE AG
Zweigniederlassung Domdidier
Route de | “Industrie 93

1564 DOMDIDIER

SCHWEIZ

Versandanschrift

ALDI SUISSE AG
Zweigniederlassung Domdidier
Route de | “Industrie 93 93
1564 DOMDIDIER

SCHWEIZ

ILN: 0041498 00019 6

KD-NR.: 201794

HANSA-HEEMANN AG
Halstenbeker Weg 98
25462 Rellingen

Telefon: 04101- 505-0
Telefax: 04101 - 505 - 160

Proformarechnung: 90253095 / 07.02.2018

 

Ordernummer

96319

Lieferscheinnummer Datum
81238041 07.02.2018
Auftragsnummer Datum
1131476 30.01.2018

Lieferscheinnummer

Kundennummer Steuernummer des Empfangers

201794.

 

 

 

Soweit nicht anders angegeben, gilt das Lieferscheindatum als Liefer-/Leistungsdatum.

LIEFERUNG
Artikel Menge Bezeichnung

 

7389204 800 6X1,50 PET-EW

11628

7390204 1.600 6X1,50 PET-EW

11628

Menge ME V_ Preis EUR
(CH) Aquata medium 4.800 FL E 00,1605 770,40
EAN: 28102656
Zolltarifnr.: 22011019
(CH) Aquata still 9.600 FL E 0,1559 1.496,64
EAN: 28102649
Zolltarifnr.: 22011011
2.267,04.
ee SOE
STPFL. BETRAG % UST. ENDBETRAG
2.267,04 0,00 2.267,04

Wir weisen auf unsere Lieferungs- und Zahlungsbedingungen hin, die Sie den Lieferscheinen entnehmen kénnen.
Vorsitzender des Aufsichtsrates: Wolff Lange;Vorstand: German Reichert (Vorsitzender), Tobias Giles-Bluhm, Thomas Reise.
Sitz der Gesellschaft, die beim Amtsgericht Pinneberg unter der Handelsregisternummer HRB 3619 eingetragen ist, ist Rellingen.

Bankverbindung:

Deutsche Bank AG, Hamburg

Swift Code DEUTDEHH

(BLZ 200 700 00) Kto.Nr. 22580500
IBAN: DE13200700000022580500

Commerzbank AG, Hamburg

Swift Code COBADEFFXXX

(BLZ 200 400 00) Kto.Nr. 408400000
IBAN : DE67200400000408400000
ILN:40 42853 00000 O

UST - IdNr.: DE 811118928 Steuernummer: 18/296/14569
