Ses 2A OGzE ,

SYNCO CHEMIE AG

SYNCO CHEMIE AG
Langmattweg 3

CH-4466 Ormalingen
SWITZERLAND

Tel. +41 (0)61 985 10 40
Fax +41 (0)61 981 14 47

Order No. 206232

FACTURE
Votre commande:15/09/2017

Votre Réf: ACR
Conditions de livraison:

Livraison DDP WERLE,68800 THANN,

TVA payée par M+R
Conditions du paiement:

DYES, PIGMENTS AND CHEMICALS

24
Metzger+Richner Transport SARL
Représentant Fiscal de
SYNCO CHEMIE AG
Rue des Transitaires
F-68305 St. Louis
France

No. 15478 25/09/2017 p:

N. Ré£: Anne-Catherine Rosenbaum

21 Faubourg des Vosges,

 
   

No TVA intracommunautaire: FR94485196323
No.Art. Description Colis Prix / kg
Lot Tarif douanier Quantité Montant CHF
4309-20 Reactif Noir MP liq. 10.000 bidon CHF 1.55
217089 3204.1600 300.000 kg CHF 465.00
Origine : Suisse, sans préf.
4083-94 Somazin Orange VS 24.000 crt. CHF 7.95
48768600 3204.1600 600.000 kg CHF 4770.00
Origine : Inde
SYNCO CHEMIE AC
Langmattwe
CH-4466 Or
errs —
TOTAL CHF 5235.00
Total Colis / Palettes Net Brut
34 div. / 2 900.000 kg 984.000 kg

Les marchandises livrées demeurent notre propriété jusqu'au

Tel +41 61 985 10 40 UBS AG, CH-4410

Fax +41 61 981 14 47 IBAN: CH20 0024
info@synco.ch IBAN: CH95 0024
CHE-107.968.280 MWST IBAN: CH15 0024

Europe Tax ID No. DE 142 301 245

SWIFT/BIC: UBSWCHZH80A

complet paiement de leur prix.
Liestal BLKB, CH-4460 Gelterkinden
5245 5136 5901 K (CHF) IBAN: CH13 0076 9016 6101 5544 0 (CHF)
5245 5136 5964 X (EUR) SWIFT/BIC: BLKBCH22
5245 5136 5960 U (USD)

ISO 9001:2015 certified
