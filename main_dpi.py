import argparse
import os
from utils.OCR_Azure import ocr as azure
from utils.OCR_Tesseract import ocr as tesseract
from utils.OCR_Vision import ocr as vision

import time

parser = argparse.ArgumentParser(description='Process input.')
parser.add_argument('--file_name', metavar='F', type=str, help="Input a PDF file")
parser.add_argument('--res', metavar='R', type=str, help="Resolution")
opt = parser.parse_args()

def process():

    start = time.time()
    txt_azure = str(azure(opt.file_name))
    end = time.time()
    azure_time = end-start
    start = time.time()
    txt_tesseract = tesseract(opt.file_name)
    end = time.time()
    tesseract_time = end-start

    start = time.time()
    txt_vision = vision(opt.file_name)
    end = time.time()
    vision_time = end - start

    file_index = opt.file_name.split('\\')[7].split('_')[0]
    f_azure = open("./Azure_Results_DPI/" + file_index + "/"  + opt.res + ".txt", "w+", encoding= 'utf-8')
    f_tesseract = open("./Tesseract_Results_DPI/"  + file_index + "/"  + opt.res + ".txt", "w+", encoding= 'utf-8')
    f_azure.write(txt_azure + "\n")
    f_tesseract.write(txt_tesseract + "\n")

    f_vision = open("./Vision_Results_DPI/"  + file_index + "/"  + opt.res + ".txt", "w+", encoding= 'utf-8')
    f_vision.write(txt_vision + "\n")

    print("RESOLUTION: ", opt.res,"AZURE TIME (s): ", azure_time,"TESSERACT TIME (s): ", tesseract_time, "VISION TIME (s):", vision_time)
    

def process_dpi(filename, dpi):

    start = time.time()
    txt_azure = str(azure(filename))
    end = time.time()
    azure_time = end-start
    start = time.time()
    txt_tesseract = tesseract(filename)
    end = time.time()
    tesseract_time = end-start

    start = time.time()
    txt_vision = vision(filename)
    end = time.time()
    vision_time = end - start


    file_index = filename.split('\\')[7].split('_')[0]
    f_azure = open("./Azure_Results_DPI/" + file_index + "/"  + dpi + ".txt", "w+", encoding= 'utf-8')
    f_tesseract = open("./Tesseract_Results_DPI/"  + file_index + "/"  + dpi + ".txt", "w+", encoding= 'utf-8')
    f_azure.write(txt_azure + "\n")

    f_vision = open("./Vision_Results_DPI/"  + file_index + "/"  + dpi + ".txt", "w+", encoding= 'utf-8')
    f_vision.write(txt_vision + "\n")

    print("RESOLUTION: ", dpi,"AZURE TIME (s): ", azure_time,"TESSERACT TIME (s): ", tesseract_time, "VISION TIME (s):", vision_time)
    f_tesseract.write(txt_tesseract + "\n")

    return (dpi,azure_time,tesseract_time,vision_time)

if __name__ == "__main__":

    process()
