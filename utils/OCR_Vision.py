from google.cloud import vision

import io
import os

os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="peri_key.json"

def ocr(path):
    
    """Detects text in the file."""

    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)

    response = client.text_detection(image=image)
    texts = response.text_annotations
    
    for text in texts:
        
        text = '\n"{}"'.format(text.description)

        return text
