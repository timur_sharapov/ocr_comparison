import requests
import sys
import time

# If you are using a Jupyter notebook, uncomment the following line.
#%matplotlib inline
#import matplotlib.pyplot as plt
#from matplotlib.patches import Rectangle
#from PIL import Image
from io import BytesIO
import os
# Add your Computer Vision subscription key and endpoint to your environment variables.
if 'COMPUTER_VISION_SUBSCRIPTION_KEY' in os.environ:
    subscription_key = os.environ['COMPUTER_VISION_SUBSCRIPTION_KEY']
else:
    print("\nSet the COMPUTER_VISION_SUBSCRIPTION_KEY environment variable.\n**Restart your shell or IDE for changes to take effect.**")
    sys.exit()

if 'COMPUTER_VISION_ENDPOINT' in os.environ:
    endpoint = os.environ['COMPUTER_VISION_ENDPOINT']

ocr_url = endpoint + "/v2.1/ocr"

def ocr(image_path):
    
    # Read the image into a byte array
    image_data = open(image_path, "rb").read()
    # Set Content-Type to octet-stream
    import http.client, urllib.request, urllib.parse, urllib.error, base64,time
    headers = {
    # Request headers
    'Content-Type': 'application/octet-stream',
    'Ocp-Apim-Subscription-Key': subscription_key,
}

    params = urllib.parse.urlencode({
    # Request parameters
    'mode': 'Printed',
})

    try:
        
        conn = http.client.HTTPSConnection('eastasia.api.cognitive.microsoft.com')
        conn.request("POST", "/vision/v2.0/recognizeText?%s" % params, image_data, headers)
        response = conn.getresponse()
        data = response.read()
        print(data)
        conn.close()

    except Exception as e:
        print("[Errno {0}] {1}".format(e.errno, e.strerror))

    analysis = {}

    while not "recognitionResult" in analysis:
    
        response_final = requests.get(
        response.headers["Operation-Location"], headers=headers)
        analysis = response_final.json()
        #print(analysis) commented 29.11.2019
        time.sleep(1)

    polygons = []
    if ("recognitionResult" in analysis):
    # Extract the recognized text, with bounding boxes.
        polygons = [(line["boundingBox"], line["text"]) for line in analysis["recognitionResult"]["lines"]]
    #print(polygons)
    return polygons
