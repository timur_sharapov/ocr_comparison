import subprocess
import os
from main_comp import process_comp

azure_results = {str(item):[] for item in range(50,101,5)}
tesseract_results = {str(item):[] for item in range(50,101,5)}
vision_results = {str(item):[] for item in range(50,101,5)}

cwd = "./input/300_"
for value in range(50,101,5):
    for item in os.listdir(cwd + str(value)):
        if item.endswith(".jpeg"):
            #subprocess.call(['python','main_comp.py','--file_name', cwd + str(value) + "/" +  item,'--comp', str(value)])
            comp, azure, tesseract,vision = process_comp(cwd + str(value) + "/" +  item,str(value))
            azure_results[comp].append(azure)
            tesseract_results[comp].append(tesseract)
            vision_results[comp].append(vision)
            print(azure_results)
            print(tesseract_results)
            print(vision_results)

azure_results = {k:(sum(v)/len(v)) for k,v in azure_results.items()}
tesseract_results = {k:sum(v)/len(v) for k,v in tesseract_results.items()}
vision_results = {k:sum(v)/len(v) for k,v in vision_results.items()}

with open("./Logs/azure_timing_comp.txt", "w+") as azure_timing:

    output = "Azure Results:\n"

    for k,v in azure_results.items():
        output = output + k + " : " + str(round(v,2)) + "\n"

    azure_timing.write(output)

with open("./Logs/tesseract_timing_comp.txt","w+") as tesseract_timing:
    
    output = "Tesseract Results:\n"

    for k,v in tesseract_results.items():
        output = output + k + " : " + str(round(v,2)) + "\n"

    tesseract_timing.write(output)

with open("./Logs/vision_timing_comp.txt","w+") as vision_timing:
    
    output = "Vision Results:\n"

    for k,v in vision_results.items():
        output = output + k + " : " + str(round(v,2)) + "\n"

    vision_timing.write(output)