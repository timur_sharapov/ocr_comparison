import subprocess
import os
from main_dpi import process_dpi

azure_results = {str(item):[] for item in range(150,300,25)}
tesseract_results = {str(item):[] for item in range(150,300,25)}
vision_results = {str(item):[] for item in range(150,300,25)}

for value in range(150,300,25):
    source_dir = os.path.join(os.getcwd(),"input",str(value) + "_70")
    for item in os.listdir(source_dir):
        if item.endswith(".jpeg"):
            #subprocess.call(['python','main_dpi.py','--file_name', os.path.join(source_dir,item),'--res', str(value)])
            dpi, azure, tesseract,vision = process_dpi(os.path.join(source_dir,item),str(value))
            azure_results[dpi].append(azure)
            tesseract_results[dpi].append(tesseract)
            vision_results[dpi].append(vision)
            print(azure_results)
            print(tesseract_results)
            print(vision_results)

azure_results = {k:(sum(v)/len(v)) for k,v in azure_results.items()}
tesseract_results = {k:(sum(v)/len(v)) for k,v in tesseract_results.items()}
vision_results = {k:(sum(v)/len(v)) for k,v in vision_results.items()}

with open("./Logs/azure_timing_dpi.txt", "w+") as azure_timing:

    output = "Azure Results:\n"

    for k,v in azure_results.items():

        output = output + k + " : " + str(round(v,2)) + "\n"

    azure_timing.write(output)

with open("./Logs/tesseract_timing_dpi.txt","w+") as tesseract_timing:
    
    output = "Tesseract Results:\n"

    for k,v in tesseract_results.items():

        output = output + k + " : " + str(round(v,2)) + "\n"

    tesseract_timing.write(output)

with open("./Logs/vision_timing_dpi.txt","w+") as vision_timing:
    
    output = "Vision Results:\n"

    for k,v in vision_results.items():

        output = output + k + " : " + str(round(v,2)) + "\n"

    vision_timing.write(output)