import os
from wand.image import Image


def convert_to_jpeg(f,r,c):
    with(Image(filename=f, resolution=r)) as source: 
        source.compression_quality = c
        images = source.sequence
        newfilename = f[:-4] + "_" + str(r) + "_" + str(c) +  '.jpeg'
        Image(images[0]).save(filename=newfilename)
    return newfilename

def generate_files():
    cwd = os.getcwd()
    files = os.listdir(cwd)
    compression_values = list(range(50,101,5))
    for item in files:
        if item.endswith(".pdf"):
            for value in compression_values:
                convert_to_jpeg(item,300,value)

if __name__ == "__main__":
    generate_files() 