import os
from wand.image import Image


def convert_to_jpeg(f,r,c):
    with(Image(filename=f, resolution=r)) as source: 
        source.compression_quality = c
        images = source.sequence
        newfilename = f[:-4] + "_" + str(r) + "_" + str(c) +  '.jpeg'
        save_directory = os.path.join(os.getcwd(),str(r) + "_70")
        Image(images[0]).save(filename=os.path.join(save_directory,newfilename))
    return newfilename

def generate_files():
    cwd = os.getcwd()
    files = os.listdir(cwd)
    dpi_values = list(range(150,301,25))
    
    for item in files:
        if item.endswith(".pdf"):
            for value in dpi_values:
                convert_to_jpeg(item,value,70)

if __name__ == "__main__":
    generate_files() 