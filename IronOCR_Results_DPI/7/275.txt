.4

H141...
HANSA-HEEMANN

HANSA—HEEMANN AG . Postfach 1353 . 25454 Rellingen HANSA-HEEMANN AG
Halstenbeker Weg 98
ALDI SUISSE AG $$$$“S‘fﬁn1 - 505 - o
Zweigniederlassung Domdidier Telefaxr 04101 - 505 -160
Route de I’lndustrie 93 .
1564 DOMDIDIER Proformarechnung . 90253095 / 010220124
SCHWEIZ
Ordernummer
96319
Lieferscheinnummer Datum
Versandanschrift 81238041 07.02.2018
ALDI SUISSE AG Auftragsnummer Datum
Zweigniederlassung Domdidier 1131476 30.01.2018
Route de l'lndustrie 93 93
1564 DOMDIDIER Lieferscheinnummer
SCHWEIZ
Kundennummer Steuernummer des Empf'angers
201794
ILN: 0041498 00019 6 KD-NR.: 201794

Soweit nicht anders angegeben, gilt das Lieferscheindatum als Liefer-lLeistungsdatum.

LIEFERUNG
Artikel Menge Bezeichnung Menge ME V Preis EUR
7389204 800 6X1.50 PET-EW (CH) Aquata medium 4.800 FL E 0.1605 770.40
EAN: 28102656
Zolltarifnr.: M11019
11628
7390204 1.600 6X1,50 PET-EW (CH) Aquata still 9.600 FL E 0.1559 1.49664
EAN: 28102649
Zolltarifnr.: 22011011
116%
2267.04
STPFL. BETRAG % UST. ENDBETRAG
2267.04 0.00 2267.04

VW weisen auf unsere Lieferungs— und Zahlungsbedingungen hin, die Sie den Lieferscheinen entnehmen kbnnen.
Vorsitzender des Aufsichtsrates; Wolff Lange;Vorstand: German Reichert (Vorsitzender), Tobias Giles-Bluhm, Thomas Reise.
Sitz der Gesellschaft, die beim Amtsgericht Pinneberg unter der Handelsregisternummer HRB 3619 eingetragen ist, ist Rellingen.

Bankverbindung:

Deutsche Bank AG, Hamburg Commerzbank AG, Hamburg

Swift Code DEUTDEHH Swift Code COBADEFFXXX

(BLZ 200 700 00) Kto.Nr. 22580500 (BLZ 200 40000) Kto.Nr. 408400000
IBAN: DE13200700000022580500 IBAN : DE67200400000408400000

ILN:40 42853 00000 O UST - IdNr.: DE 811118928 Steuernummer: 18/296/14569