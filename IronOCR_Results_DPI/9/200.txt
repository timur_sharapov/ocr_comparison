Sapa Building Systems GmbH
Einsteinstr. 61

Invoice Original
4060828074 Page? 1’1 89077 Ulm
Print Date: 06.02.2018 Telefon (O7 31) 39 84-0
unnuuunm ||| |||||| Doc. Date. 06.02.2018 mm (0-, 31. 39 84-2 41
Hydro Building Systems Switzerland AG
Hinterméttljgrasse 1
CH-5506 MAGENWIL - _ __ .
Customer: ICC0117 Hyperion Code: CNA

Switzerland

VAT Reg.No.:
Sales Office:
Sales Administ.: SUSANNA BIANCO

Phone: 00 4162 887 41 19

Fax: 00 41 62 887 41 30

The exporter of the products covered by this document (customs authorization No. DE 9650/EA/0265) declares that.
except where otherwise clearly indicated. these products are of CE preferential origin. Ulm. the (date see above).
According to formal obligation the document needs not to be undersigned.

The customs clearing has to be handled through the ZAZ-account 7731-4.

Shipment: 266250 Route: DE01C7 Delivery I Date: 4021153660/ 06.02.2018 I Ext.De!lvery: -
Ship to: Geilinger AG. Werkstrasse. 20. CH. 8401. VWnte_r01_gr. Switzerland l g _ _
Sales Order No.: 4406099946 Customer P.0rder: EB 154677 HGFZ Luzern Building Project: HFGZ Luzern
Material I DasériptiB-E I MP—G— Bty Base Price Net Total VAT
(EUR) (EUR)
10/11 W1312132 Blendrahmen W2 29 PC 13.650.00 I1000 M 3.366.02 05
8 Treatment : ACO.206P - A6 I 00 protected I Length ' 6.709 rnﬁ j _
IC B.Price with ST 13.650.00 EUR <-> 1000 M 194.300 M 2.652.20 EUR
IC B.Price for. ST 4.41 EUR <-> 1 M28 125.518 M28 553.53 EUR
IC Tf. Pr. - Markup 5.00 % 160.29 EUR
Item Sumr 3.366 02 EUR
Customs Tariff No.: 76042100 Country of Origin: European Union
20/42 w1917104 Fm—gEIprom W2 _ — 15 136 7601.85 I1000 M {043.26 05 ‘ '
S Treatment : ACO.208 - AGICO I Length . 6.500 m __
IC B.Price with ST 7,601.85 EUR <->1000 M 97,500 M 741.18 EUR
IC B.Price for. ST 5,87 EUR <-> 1 M28 42.998 M28 252.40 EUR
IC Tr. Pr. - Markup 5.00 % 49.68 EUR
Item Sum _ t _ 1.043126 EUR
Customs Tarifl No.: 76042100 Country of Origin: European Union
Items Totalr _ _ _4.409.28
Net Value 440928
VAT (D5) Net: 4409.28 0.00 % 0.00
Total Amount 1 EUR _ 4.40928
Customs Tariff No. Net Value Net Weight
76042100 4.409.28 EUR 1.056.663 KG
Packaging/Marking Pack-No. Net Weight Gross Weight
Wooden crate 342504061650492562 201.540 KG 216.540 KG
Wooden crate 342504061650493026 530.766 KG 545.766 KG
Wooden crate 342504061650493033 324.357 KG 339.357 KG
lncoterms: CPT Carriage paid to - Gerstungen Hyperion Code: DWI

Payment terms: Due on the 15m of the following month net

VAT (05) Tax-free export delivery
In terms of amount reductions we refer to the current payment and condition agreements.

Sap: Building Systems GmbH. Einsteinstraﬂe 61. 089077 Ulm
Snz def Gesellschaft Ulm. Reg -Gcncht Utm. HRB1938. Geschansfuhrel. Henri Gomez Zertmzaet: nach DIN ISO 9001 2008
USt !dN.' DEB11154077. Commcrzbank Hamburg (BLZ 200 400 00) 6197032. BICISWIFT CCBADEFFXXX. IBAN DE 05 2004 (2000 0619 7032 00