Proformarechnung' 35841 UnterkU'mi 26-09-2017 Seite: 1

Auftragsnummer: 35841

Franke Water Systems AG
H auptstrasse 57 Transportan: Strassentransport
Incoterms: DAP WITTLICHNVENGEROHR
CH 5726 Unterkulm
EORlzDE432636140627435 0001 VERZOLLT- UNVERSTEUERT

Kaplan & Bicen GbR

Unsere Referenz Belingerstrasse 57a
Sachbearbeiter: Fischer Andreas DE 54516 WittlichNVengerohr
Telefonnummer: 062 / 768 63 05 EORI DE-6466346

Fax-Nummer: 062 / 768 61 73
E-MaiI-Adresse: andreas.ﬁscher@kwc.ch

lhre Referenz
Sachbearbeiter:
Telefonnummer: 0049 65 71 95 63 67
Fax-Nummer: 0049 65 71 95 63 68

I Me-n. Artikelnr. Artikelbezeichnung Zolltariff(EU) Ur Préf Netto[kg] Einzelpreis TotalEUR l
6 3 Annaturenbestandteile aus Messing 8481.9090 1'070.0 10'700.00

TotalBrutto: 1'520.00 TEtaI'Nett'o: 1'07'o.o TotalEUR: 10700.00

Bemerkung:

Der Ausfijhrer (Erméchtigter Ausfﬂhrer; Bewilligungs—Nr. 7495/2001) der Waren, auf die sich dieses Handelspapier
bezieht, erklért, dass diese Waren, soweit nicht anders angegeben, préferenzbegﬂnstigte CE / CH Ursprungswaren
sind.

Ort und Datum
Unterkulm, 26.09.2017

Unterschrift und Name in Druckschrift

u _1Jz/Qv~za’

Andreas Fischer
Produktions-Steuerung

Franke Water Systems AG, KWC, Hauptstrasse 57, PO. Box 179
CH-5726 Unterkulm. Switzerland

Direct + 41 62 768 63 05, Phone + 41 62 768 68 68
andreas.ﬁscher@kwc.ch, www.kwc.com