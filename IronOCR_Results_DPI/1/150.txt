Proformarechnung' 35841 UnterkU'm126-09-2017 Seite: 1

Franke Water Systems AG Auftragsnummer: 35841

Hauptstrasse 57 TranspMart: Strassentransport

CH 5725 Unterkulm Incoterms: DAP WITTLICHNVENGEROHR
EORlzDE432636140627435 0001 VERZOLLT, UNVERSTEUERT

Kaplan 8. Bicen GbR

Unsere Referenz Belingerstrasse 57a
Sachbearbeiter: Fischer Andreas DE 54516 Winlich/Wengerohr
Telefonnummer: 062 I 768 63 05 EORI DE-6466346

Fax-Nummer: 062 I 768 61 73
E-MaiI-Adresse: andreas.ﬂscher@kwc.ch

lhre Referenz
Sachbearbeiter;
Telefonnummer: 0049 65 71 95 63 67
Fax-Nummer: 0049 65 71 95 63 68

I Men. Artikelnr. Anikelbezeichnung Zontarimeu; Ur Prﬁf Neno[kg] énz—eTpreis TotaIEUR I

6 3 Armaturenbestandteile aus Messing 84819090 10700 '10770000
TotalBrutto: 1'520.00 TotalNetto: 1'070.0 TotalEUR: 10700.00

Bemerkung:

Der Ausft'jhrer (Erméchtigter Ausfﬁhrer; Bewilligungs-Nr. 7495/2001) der Waren, auf die sich dieses Handelspapier
bezieht, erklért. dass diese Waren. soweit nicht anders angegeben, préferenzbegﬁnstigte CE / CH Ursprungswaren
sind.

Ort und Datum
Unterkulm, 26.09.2017

Unterschrift und Name in Druckschrift

0.1de

Andreas Fischer
Produktions-Steuerung

Franke Water Systems AG. KWC. Hauptstrasse 57. PO. Box 179
CH-5726 Unterkulm, Switzerland

Direct + 41 62 768 63 05. Phone + 41 62 768 68 68
andreas.ﬁscher@kwc.ch, www.kwc.com