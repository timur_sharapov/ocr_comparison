GSCDCD C:£3 4:3 c>€gajg '

SVNGC ©[H1EWEIHE [AG

SYNCO CHEMIE AG
Langmattweg 3

CH-4466 Ormalingen
SWITZERLAND

Tel. +41 (0)61 985 10 40
Fax +41 (0)61 981 14 47

Order NO. 206232

FACTURE

Votre commandezls/O9/2017
Votre Réf: ACR
Conditions de livraison:

Livraison DDP WERLE,688OO THANN,

TVA payée par M+R
Conditions du paiement:

IIVES.FHGWHHVTSJQNEDCWﬂﬂWHCALS

24
Metzger+Richner Transport SARL
Représentant Fiscal de
SYNCO CHEMIE AG
Rue des Transitaires

F-68305 St. Louis
France
No. 15478 25/09/2017 p. 1

N. Réf: Anne-Catherine Rosenbaum

21 Faubourg des Vosges,

No TVA intracommunautaire: FR94485196323
No Art. Description Colis Prix / kg
Lot Tarif douanier Quantité Montant CHF
4309-20 Reactif Noir MP liq. 10.000 bidon CHF 1.55
217089 3204.1600 300.000 kg CHF 465.00
Origine : Suisse, sans préf.
4083-94 Somazin Orange VS 24.000 crt. CHF 7.95
48768600 3204.1600 600.000 kg CHF 4770.00
Origine : Inde
TOTAL CHF 5235.00
Total Colis / Palettes Net Brut
34 div. / 2 900.000 kg 984.000 kg

Lee marchandises livréea demeurent notre propriété jusqu'au complet paiemenc de leur prxx.

Tel 941 61 985 10 40 UBS AG, CH-4410 Liestal
Fax ‘41 61 981 14 47
info@synco.ch

CHE-107 968.280 MWST

Europe Tax ID No. DE 142 301 245 SHIFT/BIC: UBSWCHZHBOA

IBAN: CHZO 0024 5245 5136 5901 K (CHF)
IRAN: CH95 0024 5245 5136 5964 X (EUR)

BLKB. CH>4460 Gelterkinden
IRAN: CHIS 0076 9016 6101 5544 0 (CHF)
SHIFT/BIC: BLKBCHZZ

IBAN: CH15 0024 S245 5136 5960 U (USD)

ISO 9001:2015 certified