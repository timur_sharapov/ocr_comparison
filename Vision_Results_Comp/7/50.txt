
"HLH
HANSA HEEMANN
HANSA-HEEMANN AG . Postfach 1353 . 25454 Rellingen
HANSA-HEEMANN AG
Halstenbeker Weg 98
25462 Rellingen
Telefon: 04101 - 505 - 0
Telefax: 04101 - 505 - 160
ALDI SUISSE AG
Zweigniederlassung Domdidier
Route de l'Industrie 93
Proformarechnung: 90253095 07.02.2018
1564 DOMDIDIER
SCHWEIZ
Ordernummer
96319
Lieferscheinnummer
Datum
Versandanschrift
81238041
07.02.2018
ALDI SUISSE AG
Auftragsnummer
1131476
Datum
Zweigniederlassung Domdidier
Route de l'Industrie 93 93
30.01.2018
1564 DOMDIDIER
Lieferscheinnummer
SCHWEIZ
Steuernummer des Empfängers
Kundennummer
201794
ILN: 0041498 00019 6
KD-NR.: 201794
Soweit nicht anders angegeben, gilt das Lieferscheindatum als Liefer-/Leistungsdatum.
LIEFERUNG
Artikel
Menge Bezeichnung
Menge ME V
Preis
EUR
7389204
(CH) Aquata medium
EAN: 28102656
800 6X1,50 PET-EW
4.800 FL
E 0,1605
770,40
Zolltarifnr.: 22011019
11628
7390204
1.600 6X1,50 PET-EW
(CH) Aquata still
EAN: 28102649
9.600 FL E 0,1559
1.496,64
Zolltarifnr.: 22011011
11628
2.267,04
STPFL. BETRAG
UST.
ENDBETRAG
2.267,04
0,00
2.267,04
Wir weisen auf unsere Lieferungs- und Zahlungsbedingungen hin, die Sie den Lieferscheinen entnehmen können.
Vorsitzender des Aufsichtsrates: Wolff Lange;Vorstand: German Reichert (Vorsitzender), Tobias Giles-Bluhm, Thomas Reise
Sitz der Gesellschaft, die beim Amtsgericht Pinneberg unter der Handelsregisternummer HRB 3619 eingetragen ist, ist Rellingen.
Bankverbindung:
Deutsche Bank AG, Hamburg
Swift Code DEUTDEHH
(BLZ 200 700 00) Kto.Nr. 22580500
IBAN: DE132007000000 2 2580500
Commerzbank AG, Hamburg
Swift Code CO BADEFFXXX
(BLZ 200 400 00) Kto.Nr. 408400000
IBAN : DE67200400000408400000
ILN:40 42853 00000 0
UST-IdNr.: DE 811118928
Steuernummer: 18/296/14569
"
