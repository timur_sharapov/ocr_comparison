
"Proforma-Rechnung
giifits
Belegnummer
4525980
Seite
Datum
1/2
05.09.2017
Die ganze Welt der Werbeartikel
Kundennummer
UID-Geschäftspartner
B156532
CHE101174465
Ihr Zeichen
Referenz
Weidestraße 122 b. 22083 Hamburg
Giffits GmbH
Ihr Ansprechpartner
Marcus Kelkel (marcus.kelkel@giffits.de)
Tel.: +49-40-2788201-28 Fax: 043 - 508 01 36
B1 Swiss Gastro GmbH
Zentrallager
Althardstrasse 10
Rechnungsempfänger
B1 Swiss Gastro GmbH
8105 Regensdorf
Switzerland
Mirja Classen
Limmattalstrasse 215
8049 Zürich
Switzerland
Total EUR
Preis
Menge
Beschreibung
8.240,00
2.000
4,1200
Bonbons im Werbewickel
Lieferdatum: 22.09.2017
Artikelnr.: 14079
Ursprungsland: Germany (Baden-Württemberg)
Zolltarifnummer: 17049071
Bonbon im Werbewickel aus glasklarem alternativ weißem Zellglas einzeln
gewickelt. Format: offen ca. 80 x 60 mm Geschmack: ab 25 kg Fruchtmischung
aus Zitrone, Orange, Apfel, Kirsche und Cassis. Haltbarkeit: ca. 18 Monate bei
sachgerechter Lagerung Verpackung: ca. 190 Stück / kg, 1 kg / Beutel, 5 kg / Kar
ton Lieferzeit: ca. 15 Arbeitstage nach Druckfreigabe. Express-Lieferung auf
Anfrage! Preise inkl. 3-Farbdruck, zzgl. Nebenkosten.
Geschmack: Fruchtmischung
Veredlung ohne V.: 3-farbig
78,00
78,0000
1
Ruestkosten
2
Lieferdatum: 22.09.2017
Artikelnr.: V3550548
Logo Deperado
Druckfarben 3405C grün, magenta, schwarz
Unveränderter KlischeeNr. 61722
380,00
3 Frachtkosten
380,0000
1
Lieferdatum: 22.09.2017
Artikelnr.: TO
Bankverbindung:
Swift/BIC-Code
Commerzbank AG Zürich
Tel.
043 550 15 30
Giffits GmbH
Firmenname:
COBACHZHXXX
CH51 0883 6120 6077 0000 6
Geschäftsführer
043 508 01 36
Fax.:
Thorsten Schmidt, Marcus Schulz
IBAN-Nummer:
service@giffits.ch
HRB 106954, Hamburg
E-Mail
Handelsregister:
Gerichtstand, Erfüllungsort: Hamburg-Barmbek, Germany
43/724/00701
Steuer-Nr.:
Ust-ld-Nr.
DE262054425
WEEE-Reg.Nr.:
DE68758991
"
