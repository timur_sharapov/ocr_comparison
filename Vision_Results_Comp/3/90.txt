
"Goo 0gAO6aS
SYNCO CHEMIE AG
DYES,PIGMENTS AND CHEMICALS
SYNCO CHEMIΕ AG
Langmattweg 3
CH-4466 Ormalingen
SWITZERLAND
Tel. +41 (0) 61 985 10 40
+41 (0) 61 981 14 47
Fax
24
Metzger+Richner Transport SARL
Représentant Fiscal de
SYNCO CHEMIE AG
Rue des Transitaires
F-68305 St. Louis
France
Order No. 206232
25/09/2017
FACTURE
No. 15478
р. 1
N. Réf Anne-Catherine Rosenbaum
Votre commande:15/09/2017
Votre Réf
Conditions de livraison:
ACR
Livraison DDP WERLE, 68800 THANN, 21 Faubourg des Vosges,
TVA payée par M+R
Conditions du paiement:
No TVA intracommunautaire: FR94485196323
Prix kg
Description
Colis
No.Art
Tarif douanier
Quantité
Lot
Montant CHF
Reactif Noir MP liq.
4309-20
10.000 bidon
CHF 1.55
300.000 kg
217089
3204.1600
CHF
465.00
Origine Suisse, sans préf
Somazin Orange VS
24.000 crt
600.000 kg
4083-94
CHF 7.95
48768600
3204.1600
CHF
4770.00
Origine Inde
3YNCO CHEMIE AC
Langmattweg 3
CH-4466 Ormalinaen
TOTAL
CHF
5235.00
Colis Palettes
/
Total
Net
Brut
34 div
900.000 kg
984.000 kg
2
Les marchandises livrées demeurent notre propriété jusqu'au complet paiement de leur prix
Tel +41 61 985 10 40
UBS AG, CH-4410 Liestal
BLKB, CH-4460 Gelterkinden
IBAN CH20 0024 5245 5136 5901 K (CHF)
IBAN CH13 0076 9016 6101 5544 0 (CHF)
Fax +41 61 981 14 47
IBAN CH95 0024 5245 5136 5964 X (EUR)
info@synco.ch
SWIFT/BIC: BLKBCH22
IBAN CH15 0024 5245 5136 5960 U (USD)
CHE-107.968.280 MWST
SWIFT/BIC: UBSWCHZH8 OA
Europe Tax ID No. DE 142 301 245
ISO 9001: 2015 certified
"
