
"Unterkulm: 26.09.2017
Seite: 1
Proformarechnung: 35841
Auftragsnummer: 35841
Franke Water Systems AG
Hauptstrasse 57
CH 5726 Unterkulm
Transportart: Strassentransport
Incoterms: DAP WITTLICH/WENGEROHR
VERZOLLT, UNVERSTEUERT
EORI:DE432636140627435 0001
Kaplan & Bicen GbR
Belingerstrasse 57a
DE 54516 Wittlich/Wengerohr
Unsere Referenz
Sachbearbeiter: Fischer Andreas
Telefonnummer: 062/768 63 05
EORI DE-6466346
Fax-Nummer: 062 /768 61 73
E-Mail-Adresse: andreas.fischer@kwc.ch
Ihre Referenz
Sachbearbeiter:
Telefonnummer: 0049 65 71 95 63 67
Fax-Nummer: 0049 65 71 95 63 68
Artikelbezeichnung
Zolltariff (EU)
Ur Präf Netto [kg] Einzelpreis
Men. Artikelnr.
Total EUR
Armaturenbestandteile aus Messing
6 3
8481.9090
1'070.0
10'700.00
Total Brutto:
1'520.00
Total Netto:
1'070.0 Total EUR: 10'700.00
Bemerkung:
Der Ausführer (Ermächtigter Ausführer; Bewilligungs-Nr. 7495/2001) der Waren, auf die sich dieses Handelspapier
bezieht, erklärt, dass diese Waren, soweit nicht anders angegeben, präferenzbegünstigte CE / CH Ursprungswaren
sind.
Ort und Datum
Unterkulm, 26.09.2017
Unterschrift und Name in Druckschrift
GRher
Andreas Fischer
Produktions-Steuerung
Franke Water Systems AG, KWC, Hauptstrasse 57, P.O. Box 179
CH-5726 Unterkulm, Switzerland
Direct 41 62 768 63 05, Phone 41 62 768 68 68
andreas.fischer@kwc.ch, www.kwc.com
"
