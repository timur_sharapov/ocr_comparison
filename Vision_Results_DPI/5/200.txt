
"B BRAUN
B. Braun Medical AG Seesatz 17 CH-6204 Sempach
www.bbraun.ch
Tel. 41 58 258 50 00
Fax 41 58 258 60 00
SHARING EXPERTISE
Page 1
1
Crosscompany invoice
Nr. 316233165
Date
26.09.2017
VAT no.
CHE-106.032.803 MWST
101 VAT reg. no.: DE1 13055856
VAT reg. no
DE173631497
B. Braun Melsungen AG
PO Box 11 20
Delivery no.
157099531
D-34209 MELSUNGEN
Gordana Makic
Tel 41 58 258 56 07
Fax
+41 58 258 66 07
gordana.makic@bbraun.com
Pref.
No of material
Quantity code Description of material
Unite price
Total
Purch. order 4508271918
Our order no. 4508271918/10
6606717
33 PC
EURO-PALETTE, DIN 15146/2 800X1200MM
Customs tariff no.
U
8,92
294,36
44152000
Country of origin: CH
Purch. order 4508856637
Our order no. 4508856637/10
FEKZ513
29.700 PC
U
GELOFUSINE
EP 500ML KZ
3,58
106.326,00
Customs tariff no.
30049000
Country of origin: CH
Total items
106.620,36
Final amount in CHF
106.620,36
CIP carriage and insurance paid to
Within 60 days due net
Declaration of Origin
The exporter of the products covered by this document (customs authorization No 920) declares that, except where
otherwise clearly indicated (1), hese products are of (country of origin on position level) preferential origin
(1) See column Pref. code on position level:
N no preferential origin
U =preferential origin
B.Braun Medical Ltd
place and date: CH-6204 Sempach, 26.09.2017
Subject to our general terms and conditions
Bank account:
UBS AG, CH-6020 Emmenbrücke
Clearing Nr. 288, Account: 594715.01A
IBAN CHF: CH15 0028 8288 5947 1501A
SWIFT UBSWCHZH80A
Commerzbank AG, DE-34117 Kassel
Bank: 52080080 Account: 356009000
IBAN: EUR/USD/JPY: DE15 5208 0080 0356 0090 00
SWIFT: DRESDEFF520
"
