
"WICONA
Sapa Building Systems GmbH
Invoice
Einsteinstr. 61
Original
Page:
89077 Ulm
4060828074
1/1
Telefon (07 31) 39 84-0
Telefax (07 31) 39 84-2 41
Print Date:
06.02.2018
Doc. Date:
06.02.2018
Hydro Building Systems Switzerland AG
Hintermättlistrasse 1
CH-5506 MÄGENWIL
Customer:
VAT Reg.No.:
Sales Office
Sales Administ.: SUSANNA BIANCO
Phone: 00 41 62 887 41 19
ICC0117
Hyperion Code: CNA
Switzerland
Fax:
00 41 62 887 41 30
The exporter of the products covered by this document (customs authorization No. DE 9650/EA/0265) declares that
except where otherwise clearly indicated, these products are of CE preferential origin. Ulm, the (date see above)
According to formal obligation the document needs not to be undersigned.
The customs clearing has to be handled through the ZAZ-account 7731-4
Shipment: 266250
Route: DE01C7
Delivery Date: 4021153660 06.02.2018
Ext.Delivery:
Ship to: Geilinger AG, Werkstrasse, 20, CH, 8401, Winterthur, Switzerland
Sales Order No.: 4406099946
Customer P.Order: EB 154677 HGFZ Luzern
Building Project: HFGZ Luzern
Material / Description MPG
Qty
Base Price
(EUR)
13.650,00 /1000 M
Net Total
(EUR)
3.366,02
VAT
10/11
W1312132 Blendrahmen W2
29 PC
D5
S.Treatment ACO.208P A6 CO protected/Length : 6,700 m
IC B.Price with ST
13.650,00 EUR
4,41 EUR
5,00
<>1000 M
194,300 M
125,518 M2B
2.652,20 EUR
553,53 EUR
160,29 EUR
3.366,02 EUR
IC B.Price for. ST
1 M2B
<->
IC Tr. Pr. - Markup
Item Sum
Customs Tariff No: 76042100
Country of Origin: European Union
W1917104 Flügelprofil W2
S.Treatment ACO.208 A6/CO / Length : 6,500 m
20/42
15 PC
7.601,85/1000 M
1.043,26
D5
IC B.Price with ST
<->1000 M
7.601,85 EUR
5,87 EUR
97,500 M
741,18 EUR
252.40 EUR
49,68 EUR
IC B.Price for. ST
IC Tr. Pr. Markup
1 M2B
42,998 M2B
<->
5,00
Item Sum
1.043,26 EUR
Customs Tariff No.: 76042100
Country of Origin: European Union
Items Total
4.409,28
4.409,28
0,00
Net Value
VAT (D5)
Net: 4.409,28
0,00%
Total Amount
EUR
4.409,28
Customs Tariff No
Net Weight
Net Value
76042100
4.409,28 EUR
1.056,663 KG
Packaging/Marking
Pack-No
Net Weight
Gross Weight
Wooden crate
342504061650492562
201,540 KG
216,540 KG
Wooden crate
342504061650493026
530,766 KG
545,766 KG
Wooden crate
342504061650493033
324,357 KG
339,357 KG
Incoterms: CPT Carriage paid to - Gerstungen
Hyperion Code: DWI
Payment terms: Due on the 15th of the following month net
VAT (D5
In terms of amount reductions we refer to the current payment and condition agreements
Tax-free export delivery
Sapa Bulding Systems GmbH, Einsteinstraße 61, D-89077 Ulm
Sitz der Gesellschaft: Ulm, Reg.-Gericht Ulm, HRB1938, Geschäftsführer: Henri Gomez, Zertifiziert nach DIN ISO 9001:2008
USt.IdNr. DE811154077, Commerzbank Hamburg (BLZ 200 400 00) 6197032, BICISWIFT COBADEFFXXX, IBAN DE 05 2004 0000 0619 7032 00
"
