i B. Braun Medical AG - Seesatz 17 - CH-6204 Sempach www.bbraun.ch
B? BRAMN Tel, +41 58 258 50 00 Fax +41 58 258 60 OO
SHARING EXPERTISE Page 1/ 1

Crosscompany invoice Nr. 316233165

Date 26.09.2017

VAT no. CHE-1064O32.8O3 MWST 101 / VAT reg. no.1 DE113055856
T , . 7 1

VA reg "0 0E1 363 497 B. Braun Melsungen AG

Delivery no. 157099531 p0 Box 11 20

D-34209 MELSUNGEN

Gordana Makic

Tel. 4-41 58 258 56 07
Fax +41 58 258 66 O7
gordana.makic@bbraun.com

Pref.
No of material Quantity code Description of material Unite price Total

Purch‘ order 4508271918
Our order no. 4508271918/10

6606717 33 PC U EURO»PALETTE,D1N 15146/2 8OOX1200MM 8,92 294,36
Customs tariff no.: 44152000
Country of origin: CH

Purch. order 4508856637
Our order no. 4508856637/10

FEK2513 29.700 PC U GELOFUSINE EP SOOML KZ 3,58 106326.00
Customs tariff no: 30049000
Country of origin: CH

Total items 106.620.36
Final amount in CHF 106.620,36

C1P carriage and insurance paid to
Within 60 days due net

Declaration of Origin

The exporter of the products covered by this document (customs authorization No 920) declares that, except where
otherwise clearly indicated (1), these products are of _ _ (country of origin on position level) preferential origin.

(1) See column Pref. code on position level:
N : no preferential origin
U : preferential origin

B.Eraun Medical Ltd. place and date: CH-6204 Sempach, 26.09.2017

Subject to our general terms and conditions.

Bank account: UBS AG, CH-6020 Emmenbn’jcke Commerzbank AG, DE-34117 Kassel
CIearmg Nr.: 288, Account: 594715.01A Bank: 52080080 Account: 356009000
[BAN CHF: CH15 0028 8288 5947 1501A IBAN: EUR/USD/JPY: DF15 5208 0080 0356 0090 00

SWIFT UBSWCHZH8OA SWIFT: DRESDEFFSZO