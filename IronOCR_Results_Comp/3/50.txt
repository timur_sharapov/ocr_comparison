gm “1’3 4% 0&3 ,
$VDG©© ©HEW’HHE AG DVES. PIGMENTS AND CHEMICALS

SYNCO CHEMIE AG
Langmattweg 3
CH—4466 Ormalingen

SWITZERLAND
Tel. +41 (0)61 985 10 40
Fax +41 (0)61 981 14 47 24

Metzger+Richner Transport SARL
Représentant Fiscal de

SYNCO CHEMIE AG

Rue des Transitaires

F-68305 St. Louis

France
Order No. 206232
FACTURE NO. 15478 25/09/2017 p. 1
Votre commandezlS/O9/20l7 N. Réf: Anne-Catherine Rosenbaum

Votre Réf: ACR

Conditions de livraison:
Livraison DDP WERLE,68800 THANN, 21 Faubourg des Vosges,
TVA payée par M+R

Conditions du paiement:
No TVA intracommunautaire: FR94485196323

No.Art. Description Colis Prix / kg
Lot Tarif douanier Quantité Montant CHF
4309-20 Reactif Noir MP liq. 10.000 bidon CHF 1.55
217089 3204.1600 300.000 kg CHF 465.00

Origine : Suisse, sans préf.

4083-94 Somazin Orange VS 24.000 crt. CHF 7.95
48768600 3204.1600 600.000 kg CHF 4770.00

Origine : Inde

Langmattwéﬁ 3 ,.
“H-4466/9ﬁn mqpv0\>

TOTAL CHF 5235.00
Total Colis / Palettes Net Brut
34 div. / 2 900.000 kg 984.000 kg

Les marchandises livrées demeurent notre propriété jusqu'au complet paiement de leur prix.

Tel +41 61 985 10 40 UBS AG, CH-4410 Liestal BLKB, CH-4460 Gelterkinden

Fax +41 61 981 14 47 IBAN: CH20 0024 5245 5136 5901 K (CHF) IBAN: CH13 0076 9016 6101 5544 0 (CHF)
info@synco.ch IBAN: CH95 0024 5245 5136 5964 X (EUR) SWIFT/BIC: BLKBCH22

CHE-107.968.280 MWST IBAN: CH15 0024 5245 5136 5960 U (USD)

Europe Tax ID No. DE 142 301 245 SWIFT/BIC: UBSWCHZHBOA ISO 9001:2015 certified