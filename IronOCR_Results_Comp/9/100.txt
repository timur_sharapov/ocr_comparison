Sapa Building Systems GmbH
Einsteinstr. 61

Invoice Original 89077 Ulm
4060828074 zggfbate: 0202,2013 Telefon (07 31) 39 84—0
IIIIHIIIHIHIIIIHHN Doc-Date: 060212018 16.910073113984241

Hydro Building Systems Switzerland AG
Hintermattlj’strasse 1
CH-5506 MAGENWIL

- Customer: |CCO117 Hyperion Code: CNA
Swutzerland VAT Reg.No.:
Sales Office:
Sales Administ: SUSANNA BIANCO
Phone: 00 41 62 887 41 19 Fax: 0041 62 887 41 30

The exporter of the products covered by this document (customs authorization No. DE 9650/EA/0265) declares that.
except where otherwise clearly indicated, these products are of CE preferential origin. Ulm. the (date see above).
According to formal obligation the document needs not to be undersigned.

The customs clearing has to be handled through the ZAZ-account 7731—4,

Shipment: 266250 Route: DE01C7 Delivery I Date: 4021153660/ 06022018 /

Ship to: Geilinger AG. Werkstrasse, 20. CH, 8401. Wmterthur, Switzerland
Sates Order No.: 4406099946 Customer P‘Order: EB 154677 HGFZ Luzern

Ext.DeIlvery: -

Building Project: HFGZ Luzern

Material I Description I MPG Qty Base Price Net Total VAT
(EUR) (EUR)
10/11 W1312132 Blendrahmen W2 29 PC 13.65000 [1000 M 3.36602 05
STreatment : ACO.208P - A6 I C0 protected / Length : 6,700 m
[C B.Price with ST 13.650.00 EUR <-> 1000 M 194.300 M 265220 EUR
IC B.Price for. ST 4,41 EUR <—> 1 M28 125,518 M28 553,53 EUR
IC Tr. Pr. — Markup 5,00 % 160.29 EUR

Item Sum 3.366,02 EUR

Customs Tariff No.: 76042100 Country of Origin: European Union

20/42 W1917104 Flﬂgelproﬁl W2 15 PC 7,601.85 l1000 M 104326 05
STreatment : ACO.208 - A6/C0/ Length : 6,500 m
[C B.Price with ST 7601,85 EUR <-> 1000 M 97,500 M 741,18 EUR
1C B.Price for. ST 5.87 EUR <-> 1 M28 42.998 MZB 252.40 EUR
IC Tr, Pr, - Markup 5.00 % 49.68 EUR
Item Sum 1043,26 EUR
Customs Tariff No; 76042100 Country of Origin: European Union
Items Total 4,409.28
Net Value 4409,28
VAT (D5) Net: 4,409.28 0.00 % 000
Total Amount EUR 4.409,28
Customs Tariff No. Net Value Net Weight
76042100 4409.28 EUR 1,056,663 KG
Packaging/Marking Pack-No. Net Weight Gross Weight
Wooden crate 342504061650492562 201,540 KG 216.540 KG
Wocden crate 342504061650493026 530.766 KG 545,766 KG
Wooden crate 342504061650493033 324,357 KG 339.357 KG

Incoterms: CPT Carriage paid to - Gerstungen

Payment terms: Due on the 15m of the following month net

VAT (05)

Tax-free export delivery

In terms of amount reductions we refer to the current payment and condition agreements.

Sapa Bulldtng Systems GmbH, Einstemstraue 61, D~89077 Ulm
SR2 der Gesellschaft: Ulm, Reg-Gericht Ulm. HRE1938,Geschah510hreL Henri Gomez. Zenmzien nach DIN ISO 9001:2008
USUdNL DEB11154077. Commerzbank Hamburg (BLZ 200 400 00) 5197032, BICISWIFI’ COBADEFFXXX, JEAN DE 05 2004 0000 0619 7032 00

Hyperion Code: DWI